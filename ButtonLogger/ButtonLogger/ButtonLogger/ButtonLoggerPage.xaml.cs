﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace ButtonLogger
{
    public partial class ButtonLoggerPage : ContentPage
    {
        StackLayout loggerLayout = new StackLayout();
        public ButtonLoggerPage()
        {
            //InitializeComponent();
            Button button = new Button
            {
                Text = "Log the Click Time"
            };

            button.Clicked += Button_Clicked;

            this.Padding = new Thickness(5, Device.OnPlatform(20, 0, 0), 5, 0);

            this.Content = new StackLayout
            {
                Children =
                {
                    button,
                    new ScrollView
                    {
                        VerticalOptions=LayoutOptions.Center,
                        Content=loggerLayout
                    }
                }
            };
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            loggerLayout.Children.Add(new Label
            {
                Text = "Button clicked at " + DateTime.Now.ToString("T")
            });
        }
    }
}
