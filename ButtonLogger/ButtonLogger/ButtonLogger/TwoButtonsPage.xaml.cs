﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace ButtonLogger
{
    public partial class TwoButtonsPage : ContentPage
    {
        Button addButton, removeButton,newButton;
        StackLayout loggerLayout = new StackLayout();
        public TwoButtonsPage()
        {
            //InitializeComponent();
            addButton = new Button
            {
                Text = "Add",
                HorizontalOptions = LayoutOptions.CenterAndExpand
            };
            addButton.Clicked += OnButtonClicked;
            removeButton = new Button
            {
                Text = "Remove",
                HorizontalOptions = LayoutOptions.CenterAndExpand,
                IsEnabled = false
            };

            newButton = new Button
            {
                Text = "Dodany nowy button",
                
                
                
            };
            
            removeButton.Clicked += OnButtonClicked;
            this.Padding = new Thickness(5, Device.OnPlatform(20, 0, 0), 5, 0);

            this.Content = new StackLayout
            {
                Children =
                {
                    new StackLayout
                    {
                       Orientation = StackOrientation.Horizontal,
                       Children =
                       {
                           addButton,
                           removeButton,
                           newButton
                       }
                    },

                    new ScrollView
                    {
                       VerticalOptions = LayoutOptions.FillAndExpand,
                       Content = loggerLayout
                    }
               }
            };

        }

        /* private void RemoveButton_Clicked(object sender, EventArgs e)
         {
             loggerLayout.Children.RemoveAt(0);
             //removeButton.IsEnabled = loggerLayout.Children.Count > 0;
         }

         private void AddButton_Clicked(object sender, EventArgs e)
         {
             loggerLayout.Children.Add(new Label
             {
                 Text = "Button clicked at " + DateTime.Now.ToString("T")
             });
             removeButton.IsEnabled = loggerLayout.Children.Count > 0;
         }*/

        void OnButtonClicked(object sender, EventArgs args)
       {
           Button button = (Button)sender;
           if (button == addButton)
           {
               // Add Label to scrollable StackLayout.
               loggerLayout.Children.Add(new Label
               {
                   Text = "Button clicked at " + DateTime.Now.ToString("T")
               });
           }
           else
           {
               // Remove topmost Label from StackLayout
               loggerLayout.Children.RemoveAt(0);
           }
           // Enable "Remove" button only if children are present.
           removeButton.IsEnabled = loggerLayout.Children.Count > 0;
       }
    }
}
