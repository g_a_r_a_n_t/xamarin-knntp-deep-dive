﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace HelloXamarin
{
    public partial class GreetingsPage : ContentPage
    {
        public GreetingsPage()
        {
            //InitializeComponent();
            /*Label label = new Label();
            label.Text = "Greetings, Xamarin.Forms!";
            label.HorizontalOptions = LayoutOptions.Center;
            label.VerticalOptions = LayoutOptions.Center;
            BackgroundColor = Color.Yellow;
            label.BackgroundColor = Color.Blue;
            Content = label;*/

            Content = new Label
            {
                Text = "Greetings, Xamarin.Forms!",
                HorizontalOptions = LayoutOptions.Center,
                VerticalOptions = LayoutOptions.Center,
                BackgroundColor = Color.Blue

            };


        }

         
                
    }
}
